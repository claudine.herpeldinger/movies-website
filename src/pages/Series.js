import axios from 'axios';
import { useEffect, useState } from 'react';

import Carousel from '../components/Carousel';
import Search from '../components/Search';
import ResultsSearch from '../components/ResultsSearch';

function Series() {
    const [popularSeries, setPopularSeries] = useState([]);
    const [genres, setGenres] = useState([]);
    const [topRatedSerie, setTopRatedSerie] = useState([]);
    const [searchedSeries, setSearchedSeries] = useState([]);
    const [hasResultsSearch, setHasResultsSearch] = useState(true);

    useEffect(() => {
        const fetchData = async () => {
            const resPopular = await axios.get(`https://api.themoviedb.org/3/tv/popular?api_key=4c674a53671a795051cdc632fc7c11c4`);
            const resGenres = await axios.get(`https://api.themoviedb.org/3/genre/tv/list?api_key=4c674a53671a795051cdc632fc7c11c4`);
            const resTopRatedSeries =  await axios.get(`https://api.themoviedb.org/3/tv/top_rated?api_key=4c674a53671a795051cdc632fc7c11c4`);

            // add genre to popular series
            const updatedSeries = resPopular.data.results.map((serie) => {
                return {
                    ...serie,
                    genres: serie.genre_ids.map((genreId) => {
                        return resGenres.data.genres.find((genre) => {
                           return genre.id === genreId
                        }).name
                    })
                }
            });

            // add genre to top rated series
            const updatedTopSeries = resTopRatedSeries.data.results.map((topSerie) => {
                return {
                    ...topSerie,
                    genres: topSerie.genre_ids.map((genreId) => {
                        return resGenres.data.genres.find((genre) => {
                           return genre.id === genreId
                        }).name
                    })
                }
            });

            setPopularSeries(updatedSeries);
            setGenres(resGenres.data.genres);
            setTopRatedSerie(updatedTopSeries)

        }

        fetchData();
    }, [])

    const searchSeries = async (text) => {
        setHasResultsSearch(true);

        // empty array if not text 
        if(!text) {
            setSearchedSeries([])
            return
        }

        const resSearchSeries = await axios.get(`https://api.themoviedb.org/3/search/tv?api_key=4c674a53671a795051cdc632fc7c11c4&language=en-US&page=1&include_adult=false&query=${text}`);

        // add genre to series
        const updatedSeries = resSearchSeries.data.results.map((serie) => {
            return {
                ...serie,
                genres: serie.genre_ids.map((genreId) => {
                    return genres.find((genre) => {
                       return genre.id === genreId
                    }).name
                })
            }
        });

        setSearchedSeries(updatedSeries)
        // no result
        if (updatedSeries.length === 0) {
            setHasResultsSearch(false);
        }

    }

    return (

        <div>
            <Search 
                handleSearch={searchSeries}/>
            {searchedSeries.length > 0 && 
                <ResultsSearch data={searchedSeries} type="series"/>
            }
            {hasResultsSearch === false &&
                <p>Film not found</p>
            }
            <Carousel data={popularSeries} type="series"/>
            <Carousel data={topRatedSerie} type="top-series"/>
        </div>
    )
}

export default Series;