import axios from 'axios';
import { useEffect, useState } from 'react';

import Search from '../components/Search';
import Carousel from '../components/Carousel';
import ResultsSearch from '../components/ResultsSearch';


function Movies() {
    const [popularMovies, setPopularMovies] = useState([]);
    const [genres, setGenres] = useState([]);
    const [topRatedMovie, setTopRatedMovie] = useState([]);
    const [searchedMovies, setSearchedMovies] = useState([]);
    const [hasResultsSearch, setHasResultsSearch] = useState(true);

    useEffect(() => {
        const fetchData = async () => {
            const resPopular = await axios.get(`https://api.themoviedb.org/3/movie/popular?api_key=4c674a53671a795051cdc632fc7c11c4`);
            const resGenres = await axios.get(`https://api.themoviedb.org/3/genre/movie/list?api_key=4c674a53671a795051cdc632fc7c11c4`);
            const resTopRatedMovies =  await axios.get(`https://api.themoviedb.org/3/movie/top_rated?api_key=4c674a53671a795051cdc632fc7c11c4`);

            // add genre to popular movies
            const updatedMovies = resPopular.data.results.map((movie) => {
                return {
                    ...movie,
                    genres: movie.genre_ids.map((genreId) => {
                        return resGenres.data.genres.find((genre) => {
                           return genre.id === genreId
                        }).name
                    })
                }
            });

            // add genre to top rated movies
            const updatedTopMovies = resTopRatedMovies.data.results.map((topMovie) => {
                return {
                    ...topMovie,
                    genres: topMovie.genre_ids.map((genreId) => {
                        return resGenres.data.genres.find((genre) => {
                           return genre.id === genreId
                        }).name
                    })
                }
            });
            
            setPopularMovies(updatedMovies);
            setGenres(resGenres.data.genres);
            setTopRatedMovie(updatedTopMovies)
        }

        fetchData();
    }, [])

    const searchMovies = async (text) => {
        setHasResultsSearch(true);

        // empty array if not text 
        if(!text) {
            setSearchedMovies([])
            return
        }

        const resSearchMovies = await axios.get(`https://api.themoviedb.org/3/search/movie?api_key=4c674a53671a795051cdc632fc7c11c4&language=en-US&page=1&include_adult=false&query=${text}`);

        // add genre to movies
        const updatedMovies = resSearchMovies.data.results.map((movie) => {
            return {
                ...movie,
                genres: movie.genre_ids.map((genreId) => {
                    return genres.find((genre) => {
                       return genre.id === genreId
                    }).name
                })
            }
        });

        setSearchedMovies(updatedMovies)
        // no result
        if (updatedMovies.length === 0) {
            setHasResultsSearch(false);
        }

    }

    return (

        <div>
            <Search 
                handleSearch={searchMovies}/>
            {searchedMovies.length > 0 && 
                <ResultsSearch data={searchedMovies} type="movies" />
            }
            {hasResultsSearch === false &&
                <p>Film not found</p>
            }
            <Carousel data={popularMovies} type="movies"/>
            <Carousel data={topRatedMovie} type="top-movies"/>
        </div>
    )
}

export default Movies;