import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import Movies from './pages/Movies';
import Series from './pages/Series';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <BrowserRouter>
    <Routes>
        <Route path="/" element={<App />}>
          <Route path="movies" element={<Movies />} />
          <Route path="series" element={<Series />} />
        </Route>
    </Routes>
    </BrowserRouter>
  </React.StrictMode>
);

reportWebVitals();
