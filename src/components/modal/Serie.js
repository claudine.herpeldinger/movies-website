import { useEffect, useState } from 'react';
import axios from 'axios';
import dayjs from 'dayjs';

import Similar from '../Similar';
import Episodes from '../Episodes';

function Serie({ isOpen, serieInfo, onClose }) {

    const [crew, setCrew] = useState([]);
    const [cast, setCast] = useState([]);
    const [similarSeries, setSimilarSeries] = useState([]);
    const [serieDetails, setSerieDetails] = useState([]);
    const [episodes, setEpisodes] = useState([]);

    useEffect(() => {
        if (serieInfo.id) {
            axios.get(`https://api.themoviedb.org/3/tv/${serieInfo.id}/credits?api_key=4c674a53671a795051cdc632fc7c11c4&language=en-US`).then(res => {
                setCrew(res.data.crew)
            });
            axios.get(`https://api.themoviedb.org/3/tv/${serieInfo.id}/credits?api_key=4c674a53671a795051cdc632fc7c11c4&language=en-US`).then(res => {
                setCast(res.data.cast)
            });
            axios.get(`https://api.themoviedb.org/3/tv/${serieInfo.id}/similar?api_key=4c674a53671a795051cdc632fc7c11c4&language=en-US`).then(res => {
                setSimilarSeries(res.data.results)
            })
            axios.get(`https://api.themoviedb.org/3/tv/${serieInfo.id}?api_key=4c674a53671a795051cdc632fc7c11c4&language=en-US`).then(res => {
                setSerieDetails(res.data)
                axios.get(`https://api.themoviedb.org/3/tv/${serieInfo.id}/season/1?api_key=4c674a53671a795051cdc632fc7c11c4&language=en-US`).then(res => {
                    setEpisodes(res.data.episodes)
                })
            })
        }

    }, [serieInfo])

    if (!isOpen || !serieInfo) {
        return <></>
    }

    const genres = serieInfo.genres.join(",")
    const releaseDate = dayjs(serieInfo.release_date).format('YYYY');
    console.log("episodes", episodes)

    return (
        <div className="overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 w-full md:inset-0 h-modal md:h-full">
            <div className="relative p-4 w-full max-w-5xl h-full md:h-auto">
                <div className="relative bg-white rounded-lg shadow bg-zinc-900">
                    <div className="flex justify-between items-start p-4 rounded">
                        <h3 className="text-xl font-semibold">
                            {serieInfo.name}
                        </h3>
                        <button onClick={() => onClose()} type="button" className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white" data-modal-toggle="defaultModal">
                            <svg aria-hidden="true" className="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                            <span className="sr-only">Close modal</span>
                        </button>
                    </div>
                    {/* TODO: add average */}
                    <div className="ml-4">
                        <div class="flex items-center">
                            <svg aria-hidden="true" class="w-5 h-5 text-yellow-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>First star</title><path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path></svg>
                            <svg aria-hidden="true" class="w-5 h-5 text-yellow-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Second star</title><path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path></svg>
                            <svg aria-hidden="true" class="w-5 h-5 text-yellow-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Third star</title><path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path></svg>
                            <svg aria-hidden="true" class="w-5 h-5 text-yellow-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Fourth star</title><path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path></svg>
                            <svg aria-hidden="true" class="w-5 h-5 text-gray-300 dark:text-gray-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Fifth star</title><path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path></svg>
                        </div>
                    </div>
                    <div className="flex flex-row justify-between p-4">
                        <div className="basis-1/5">
                            <img src={`https://image.tmdb.org/t/p/w200${serieInfo.poster_path}`} alt={serieInfo.name} />
                        </div>
                        <div className="basis-1/2">
                            <p className="text-base leading-relaxed text-white-500">
                                {serieInfo.overview}
                            </p>
                        </div>
                        <div className="basis-1/4">
                            <p className="text-xl text-gray-900 dark:text-white">{releaseDate}</p>
                            <p className="mb-5">{serieDetails.number_of_seasons} saisons</p>

                            <div className="mb-5">
                                <p className="text-sm text-slate-500">Distribution: </p>
                                {cast.slice(0, 5).map((cast) => {
                                    return (
                                        <p className="text-sm">{cast.name}</p>
                                    )
                                })}
                                <a href="#about">
                                    <p className="text-sm underline">more ...</p>

                                </a>
                            </div>
                            <p className="text-sm text-slate-500">Genres:</p>
                            <p className="text-sm">{genres}</p>
                        </div>
                    </div>
                    <div>
                        <Episodes episodes={episodes} nbSeasons={serieDetails.number_of_seasons} />
                    </div>
                    <div className="flex justify-between items-start p-4 rounded-t dark:border-gray-600">
                        <Similar type="series" similar={similarSeries} />
                    </div>
                    <div id="about" className="p-4 flex-col">
                        <h3 className="text-xl font-semibold mb-6">
                            About {serieInfo.name}
                        </h3>
                        <div className="flex-initial mb-4">
                            <p className="text-sm"><span className="text-sm text-slate-500">Director: </span>
                                {crew.map((member) => {
                                    if (member.job == "Director") {
                                        return member.name
                                    }
                                })}
                            </p>
                        </div>
                        <div className="flex-initial mb-4">
                            <p className="text-sm">
                                <span className="text-sm text-slate-500">Distribution: </span>
                                {cast.map((cast) => {
                                    return (
                                        cast.name
                                    )
                                }).join(",")}
                            </p>
                        </div>
                        {/* TODO: virgule entre les names */}
                        <div className="flex-initial mb-4">
                            <p className="text-sm"><span className="text-sm text-slate-500">Producers: </span>
                                {crew.map((member) => {
                                    if (member.job == "Producer") {
                                        return (
                                            member.name
                                        )
                                    }
                                })}
                            </p>
                        </div>
                        <div className="flex-initial">
                            <p className="text-sm"><span className="text-sm text-slate-500">Genres: </span>{genres}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Serie;