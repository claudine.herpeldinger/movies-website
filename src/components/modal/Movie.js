import { useEffect, useState } from 'react';
import axios from 'axios';
import Similar from '../Similar';
import dayjs from 'dayjs';

function Movie({ isOpen, movieInfo, onClose }) {

    const [crew, setCrew] = useState([]);
    const [cast, setCast] = useState([]);
    const [similarMovies, setSimilarMovies] = useState([]);

    useEffect(() => {
        axios.get(`https://api.themoviedb.org/3/movie/${movieInfo.id}/credits?api_key=4c674a53671a795051cdc632fc7c11c4&language=en-US`).then(res => {
            setCrew(res.data.crew)
        });
        axios.get(`https://api.themoviedb.org/3/movie/${movieInfo.id}/credits?api_key=4c674a53671a795051cdc632fc7c11c4&language=en-US`).then(res => {
            setCast(res.data.cast)
        });
        axios.get(`https://api.themoviedb.org/3/movie/${movieInfo.id}/similar?api_key=4c674a53671a795051cdc632fc7c11c4&language=en-US`).then(res => {
            setSimilarMovies(res.data.results)
        })
    }, [movieInfo])

    if (!isOpen || !movieInfo) {
        return <></>
    } 

    const genres = movieInfo.genres.join(",")
    const releaseDate = dayjs(movieInfo.release_date).format('YYYY');

    return (

        <div className="overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 w-full md:inset-0 h-modal md:h-full">
            <div className="relative p-4 w-full max-w-5xl h-full md:h-auto">
                <div className="relative bg-white rounded-lg shadow bg-zinc-900">
                    <div className="flex justify-between items-start p-4 rounded">
                        <h3 className="text-xl font-semibold">
                            {movieInfo.title}
                        </h3>
                        <button onClick={() => onClose()} type="button" className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white" data-modal-toggle="defaultModal">
                            <svg aria-hidden="true" className="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                            <span className="sr-only">Close modal</span>
                        </button>
                    </div>
                    <div className="flex flex-row justify-between p-4">
                        <div className="basis-1/5">
                            <img src={`https://image.tmdb.org/t/p/w200${movieInfo.poster_path}`} alt={movieInfo.title} />
                        </div>
                        <div className="basis-1/2">
                            <p className="text-base leading-relaxed text-white-500">
                                {movieInfo.overview}
                            </p>
                        </div>
                        <div className="basis-1/4">
                            <p className="text-xl text-gray-900 dark:text-white mb-5">{releaseDate}</p>
                            <div className="mb-5">
                                <p className="text-sm text-slate-500">Distribution: </p>
                                {cast.slice(0, 5).map((cast) => {
                                    return (
                                        <p className="text-sm">{cast.name}</p>
                                    )
                                })}
                                <a href="#about">
                                    <p className="text-sm underline">more ...</p>

                                </a>
                            </div>
                            <p className="text-sm text-slate-500">Genres:</p>
                            <p className="text-sm">{genres}</p>
                        </div>
                    </div>
                    <div className="flex justify-between items-start p-4 rounded-t dark:border-gray-600">
                        <Similar type="movies" similar={similarMovies} />
                    </div>
                    <div id="about" className="p-4 flex-col">
                        <h3 className="text-xl font-semibold mb-6">
                            About {movieInfo.title}
                        </h3>
                        <div className="flex-initial mb-4">
                            <p className="text-sm"><span className="text-sm text-slate-500">Director: </span>
                                {crew.map((member) => {
                                    if (member.job == "Director") {
                                        return member.name
                                    }
                                })}
                            </p>
                        </div>
                        <div className="flex-initial mb-4">
                            <p className="text-sm">
                                <span className="text-sm text-slate-500">Distribution: </span>
                                {cast.map((cast) => {
                                    return (
                                        cast.name
                                    )
                                }).join(",")}
                            </p>
                        </div>
                        {/* TODO: virgule entre les names */}
                        <div className="flex-initial mb-4">
                            <p className="text-sm"><span className="text-sm text-slate-500">Producers: </span>
                                {crew.map((member) => {
                                    if (member.job == "Producer") {
                                        return (
                                            member.name
                                        )
                                    }
                                })}
                            </p>
                        </div>
                        <div className="flex-initial">
                            <p className="text-sm"><span className="text-sm text-slate-500">Genres: </span>{genres}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Movie;
