import dayjs from 'dayjs';

function Similar(props) {

    return (
        <div>
            <h3 className="text-xl font-semibold mb-6">
            Similar titles
            </h3>
            {props.similar.map((similar) => {
                const releaseDate = dayjs(similar.release_date).format('YYYY');

                return (
                    <div className="grid">
                        <div className="items-center shadow sm:flex bg-zinc-800 border-b dark:border-gray-600">
                            <a href="#">
                                <img src={`https://image.tmdb.org/t/p/w200${similar.poster_path}`} />
                            </a>
                            <div className="p-5">
                                <h3 className="text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                    <a href="#"> {props.type === "movies" ? similar.title : similar.name}</a>
                                </h3>
                                <p className="text-base text-gray-900 dark:text-white">{releaseDate}</p>
                                <p className="mt-3 mb-4 font-light text-gray-500 dark:text-gray-400">{similar.overview}</p>
                                <button type="button" className="text-gray-900 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-gray-800 dark:text-white dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700">
                                    See more
                                </button>
                            </div>
                        </div>
                    </div>
                );
            })}
        </div>
    )
}

export default Similar;