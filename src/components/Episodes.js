function Episodes({episodes, nbSeasons}) {


    return (
        <div>
            <div className="flex flex-row justify-between p-4">
                <h3 className="text-xl font-semibold mb-6">
                    Episodes
                </h3>
                {/* <div className="basis-1/2">
                    <select id="countries" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        {
                            nbSeasons.map((saison) => {
                                return (
                                    <option value={saison}>Saison {saison}</option>
                                )
                            })
                        }
                    </select>
                </div> */}
            </div>
            {episodes.map((episode) => {
                return (
                    <div className="grid">
                        <div className="items-center shadow sm:flex bg-zinc-800 border-b dark:border-gray-600">
                            <div className="basis-16 text-center">
                                <p>{episode.episode_number}</p>
                            </div>
                            {/* TODO add image episode */}
                            <div className="p-5">
                                <h3 className="text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                                    <a href="#"> {episode.name}</a>
                                </h3>
                                <p className="mt-3 mb-4 font-light text-gray-500 dark:text-gray-400">{episode.overview}</p>
                                <button type="button" className="text-gray-900 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-gray-800 dark:text-white dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700">
                                    See more
                                </button>
                            </div>
                        </div>
                    </div>
                )
            })}
        </div>
    )
}

export default Episodes;