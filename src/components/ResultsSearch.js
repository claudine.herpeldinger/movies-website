import { useState } from 'react';

import Movie from "../components/modal/Movie";
import Serie from "./modal/Serie";


function ResultsSearch(props) {

    const [openModal, setOpenModal] = useState(false);
    const [data, setData] = useState({});

    const onOpenModal = (_data) => {
        setData(_data)
        setOpenModal(!openModal)
    }

    return (
        <div className="flex flex-row flex-wrap justify-between m-8">
            {props.data.map((movie) => {
                return (
                    <div className="mb-8" onClick={() => onOpenModal(movie)}>
                        <img src={`https://image.tmdb.org/t/p/w200${movie.poster_path}`} />
                    </div>
                )
            })}
            {
                props.type === "movies" ?
                    <Movie
                        movieInfo={data}
                        isOpen={openModal}
                        onClose={() => setOpenModal(false)}
                    /> :
                    <Serie
                        serieInfo={data}
                        isOpen={openModal}
                        onClose={() => setOpenModal(false)}
                    />

            }
        </div>
    )
}

export default ResultsSearch;