import { Link } from "react-router-dom";

function Header() {
  return (
<div className="relative bg-black">
  <div className="mx-auto max-w-7xl">
    <div className="flex items-center justify-between py-6 md:justify-start md:space-x-10">
      <div className="flex justify-start lg:w-0 lg:flex-1">
        <a>
          <span className="sr-only">Your Company</span>
          <img className="h-8 w-auto sm:h-10" src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600" alt=""></img>
        </a>
      </div>

      <nav className="hidden space-x-10 md:flex">

        <Link to="/movies" className="text-base font-medium text-white">Movies</Link>
        <Link to="/series" className="text-base font-medium text-white">Series</Link>

      </nav>
    </div>
  </div>
</div>

  );
}

export default Header;
